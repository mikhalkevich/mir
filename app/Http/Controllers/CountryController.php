<?php

namespace App\Http\Controllers;

use App\Parser\GoogleNewsParse;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\City;
use App\Models\Capital;

class CountryController extends Controller
{
    protected $data;

    public function __construct()
    {
    }

    public function getName($url = null)
    {
        $obj = Country::where('alpha3', $url)->first();

        return view('country', compact('obj'));
    }

    public function postAjaxCities(Request $request)
    {
        $states_obj = $this->states();
        $states     = [];
        foreach ($states_obj['countries'] as $i => $v) {
            if ($v['alpha3'] == $request->alpha3) {
                $states = $v['states'];
            }
        }

        return view('ajax.cities', compact('states'));
    }

    public function postAjaxNews(Request $request)
    {
        if (isset($request->alpha3)) {
            $alpha3 = $request->alpha3;
        } else {
            $alpha3 = 'BLR';
        }
        $country = Country::where('alpha3', $alpha3)->first();
        $obj     = new GoogleNewsParse;
        $obj->getParse($country);
    }

    public function postAjaxLinks(Request $request){
        $country = Country::where('alpha3', $request->alpha3)->first();
        return view('ajax.links', compact('country'));
    }

    private function states()
    {
        $path = config_path().'/countries_states.json';
        $json = file_get_contents($path);
        $data = json_decode($json, true);

        return $data;
    }
}
