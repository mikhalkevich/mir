<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Calendar;
use DateTime;
use DateInterval;
use App\Models\Event;

class EventController extends Controller
{
    public function getIndex()
    {
        $events = Event::all();
        return view('events', compact('events'));
    }

    public function getOne(Event $event){
        return view('event', compact('event'));
    }

}
