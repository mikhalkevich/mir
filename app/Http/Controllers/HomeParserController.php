<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;
use App\Parser\GoogleNewsParse;

class HomeParserController extends Controller
{
    public function getIndex(){
        $countries = Country::orderBy('english')->get();
        return view('home_parser', compact('countries'));
    }

}
