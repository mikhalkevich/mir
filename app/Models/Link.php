<?php

namespace App\MOdels;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    public $fillable = ['ico', 'type', 'user_id', 'name', 'url', 'country_id'];

    public function countries(){
        return $this->belongsTo(Country::class, 'country_id');
    }
}
