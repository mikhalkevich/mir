$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'post',
        success: function (data) {
            $('.empty').html(data);
        },
        error: function (data) {
            console.log(data);
        }
    });
    const alpha3 = $('#nav-alpha3').attr('data-alpha3');
    $('#link_news').click(function(){
        $.ajax({
            url: '/country/ajax/news',
            data: 'alpha3='+alpha3,
        });
    });
    $('#nav-cities-tab').click(function(){

        $.ajax({
            url: '/country/ajax/cities',
            data: 'alpha3='+alpha3,
            success: function(data){
                $('#empty_cities').html(data);
            }
        });
    });
    $('#nav-news-tab').click(function(){
        $.ajax({
            url: '/country/ajax/news',
            data: 'alpha3='+alpha3,
            success: function(data){
                $('#empty_news').html(data);
            }
        });
    });
    $('#nav-links-tab').click(function(){
        console.log(alpha3);
        $.ajax({
            url: '/country/ajax/links',
            data: 'alpha3='+alpha3,
            success: function(data){
                $('#empty_links').html(data);
            }
        });
    });
    $('#link_links').click(function(){
        country_id = $(this).attr('data-country');
        console.log(country_id);
        $.ajax({
            url: '/ajax/links',
            data: 'country_id='+country_id,
            success: function (data) {
                $('.empty_links').html(data);
            },
        });
    });
});
