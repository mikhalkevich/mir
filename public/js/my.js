const container = document.getElementById('container');
var map = new Datamap({
    element: container,
    geographyConfig: {
        highlightBorderColor: '#bada55',
        popupTemplate: function (geography, data) {

        },
        highlightBorderWidth: 3
    }
});
$(function(){
    $('path').click(function () {
        url = $(this).attr('class');
        var res = url.split(" ");
        document.location.href = '/country/' + res[1];
    });
});
