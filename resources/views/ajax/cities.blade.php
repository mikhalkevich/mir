<ul>
    @if(isset($states))
        @foreach($states as $one)
            <li>
                <a href="{{asset('city/'.$one)}}">
                    {{$one}}
                </a>
            </li>
        @endforeach
    @endif
</ul>
