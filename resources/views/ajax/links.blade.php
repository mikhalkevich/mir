<table width="100%" class="table table-bordered table-hover table-striped">
    <tr>
        <th>{{__('messages.name')}}</th>
        <th>{{__('messages.link')}}</th>
        <th>{{__('messages.catalog')}}</th>
    </tr>
    @foreach($country->links as $one)
        <tr>
            <td>{{$one->name}}</td>
            <td>{{$one->url}}</td>
            <td><a href="#">{{$one->type}}</a></td>
        </tr>
    @endforeach
</table>
