@extends('layouts.base')
@push('scripts')
    <script src="{{asset('plugins/datamaps/d3.min.js')}}"></script>
    <script src="{{asset('plugins/datamaps/topojson.min.js')}}"></script>
    <script src="{{asset('plugins/datamaps/dist/datamaps.world.min.js')}}"></script>
    <script src="{{asset('js/my.js')}}"></script>
    <script src="{{asset('js/country.js')}}"></script>
@endpush

@section('content')

    <div id="modal" class="my_modal" tabindex="1">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{($lang == 'Rus')?$obj->name:$obj->english}}</h5>
                    <a href="/" class="btn-close" aria-label="Close"></a>
                </div>
                <div class="modal-body">

                    @include('templates.country')
                </div>
            </div>
        </div>
    </div>

    <div id="container"></div>
@endsection
