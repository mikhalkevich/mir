@extends('layouts.base')
@push('styles')
    <link href='/fullcalendar/packages/core/main.css' rel='stylesheet' />
    <link href='/fullcalendar/packages/daygrid/main.css' rel='stylesheet' />
@endpush
@push('scripts')
    <script src="/js/popper.min.js"></script>

    <script src='/fullcalendar/packages/core/main.js'></script>
    <script src='/fullcalendar/packages/interaction/main.js'></script>
    <script src='/fullcalendar/packages/daygrid/main.js'></script>
    <script src='/fullcalendar/packages/timegrid/main.js'></script>
    <script src='/fullcalendar/packages/list/main.js'></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
                height: 'parent',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
                },
                defaultView: 'dayGridMonth',
                defaultDate: '{{date('Y-m-d')}}',
                navLinks: true, // can click day/week names to navigate views
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                events: [
                    @foreach($events as $event)
                    {
                        @php
                            $start_date = new DateTime($event->date_start);
                            $end_date = new DateTime($event->date_start.' +'.$event->days.' day');
                            $start_date_str = $start_date->format('Y-m-d');
                            $end_date_str = $end_date->format('Y-m-d');
                        @endphp
                        groupId: {{$event->country_id}},
                        title: '{{$event->name}}',
                        start: '{{$start_date_str}}',
                        end: '{{$end_date_str}}',
                        url: '/event/{{$event->id}}'
                    },
                    @endforeach
                ]
            });
            calendar.render();
        });

    </script>
@endpush
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id='calendar-container'>
                    <div id='calendar'></div>
                </div>
            </div>
        </div>
    </div>
@endsection
