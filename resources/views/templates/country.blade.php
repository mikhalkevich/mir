<nav>
    <div class="nav nav-tabs" id="nav-alpha3" role="tablist" data-alpha3="{{$obj->alpha3}}">
        <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">{{__('messages.events')}}</button>
        <button class="nav-link" id="nav-news-tab" data-bs-toggle="tab" data-bs-target="#nav-news" type="button" role="tab" aria-controls="nav-news" aria-selected="false">{{__('messages.news')}}</button>
        <button class="nav-link" id="nav-cities-tab" data-bs-toggle="tab" data-bs-target="#nav-cities" type="button" role="tab" aria-controls="nav-cities" aria-selected="false">{{__('messages.cities')}}</button>
        <button class="nav-link" id="nav-links-tab" data-bs-toggle="tab" data-bs-target="#nav-links" type="button" role="tab" aria-controls="nav-lnks" aria-selected="false">{{__('messages.links')}}</button>
        <button class="nav-link" id="nav-info-tab" data-bs-toggle="tab" data-bs-target="#nav-info" type="button" role="tab" aria-controls="nav-info" aria-selected="false">{{__('messages.info')}}</button>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <div class="alert" id="continent_code">
            <table class="table table-bordered table-hover">
                <tr>
                    <td width="200px">{{__('messages.picture')}}</td>
                    <td>{{__('messages.event_name')}}</td>
                    <td>{{__('messages.locale')}}</td>
                    <td>{{__('messages.date_start')}}</td>
                </tr>
                @foreach($obj->events->where('date_start','>',now()) as $event)
                    <tr>
                        <td width="200px">
                            @if($event->picture)
                                <img src="{{url('/storage/uploads/'.$event->user_id.'/s_'.$event->picture)}}" width="200px" class="float_pic"/>
                            @else
                                <img src="{{url('/img/event.webp')}}" width="200px" class="float_pic"/>
                            @endif
                        </td>
                        <td><a href="{{asset('event/'.$event->id)}}">{{$event->name}}</a></td>
                        <td>{{optional($event->city)->name}} {{$event->address}}</td>
                        <td>{{$event->date_start}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-news" role="tabpanel" aria-labelledby="nav-news-tab">
        <div id="empty_news">{{__('messages.please')}}</div>
    </div>
    <div class="tab-pane fade" id="nav-cities" role="tabpanel" aria-labelledby="nav-cities-tab">
        <div id="empty_cities"></div>
    </div>
    <div class="tab-pane fade" id="nav-links" role="tabpanel" aria-labelledby="nav-links-tab">
        <div id="empty_links"></div>
    </div>
    <div class="tab-pane fade" id="nav-info" role="tabpanel" aria-labelledby="nav-info-tab">
        <table class="table table-bordered table-hover">
            <tr>
                <td>{{__('messages.continent')}}</td>
                <td>{{__('messages.country')}}</td>
                <td>Alpha2</td>
                <td>Alpha3</td>
                <td>ISO</td>
            </tr>
            <tr>
                <td>
                    <a href="{{asset('continent/'.$obj->location)}}">
                        {{$obj->location}}
                    </a>
                </td>
                <td>
                    <a href="{{asset('country/'.$obj->english)}}">
                        {{$obj->name}} <br />
                        {{$obj->fullname}} / {{$obj->english}}
                    </a>
                </td>
                <td>{{$obj->alpha2}}</td>
                <td>{{$obj->alpha3}}</td>
                <td>{{$obj->iso}}</td>
            </tr>
        </table>
        <br/>


        <table width="100%" id="maket">
            <tr>
                <td align="center">
                    <h1 id="name">
                        {{$obj->name}}
                    </h1>
                    <div align="center">
                        <img src="{{asset('/img/flags-normal/'.strtolower($obj->alpha2).'.png')}}"/>
                        <p><br></p>
                    </div>
                </td>
            </tr>
        </table>
        {!! $obj->map_iframe  !!}
        <div align="right">
            <a href="https://google.{{$obj->alpha3}}" target="_blank">
                Google.{{$obj->alpha3}}
            </a>
            <a href="https://www.google.com/maps/place/{{$obj->name}}" target="_blank">
                Google map
            </a>
        </div>
        <br style="clear: both;">
        <div class="text">
        </div>
    </div>
</div>

