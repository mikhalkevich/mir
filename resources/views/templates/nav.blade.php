<nav class="navbar navbar-expand-lg navbar-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">MIR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{asset('/catalog')}}">
                        {{__('messages.catalog')}}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{asset('/events')}}">{{__('messages.events')}}</a>
                </li>
                @if (Auth::guest())
                    <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">{{__('messages.register')}}</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">{{__('messages.login')}}</a></li>
                @else
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        {{Auth::user()->name}}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @include('includes.home_menu')
                        <li>
                            @if(Auth::user())
                                <a href="{{ route('logout') }}" class="dropdown-item language"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            @endif
                        </li>
                    </ul>
                </li>
                @endif
            </ul>
            <div class="d-flex">
                <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <img id="imgNavSel" src="{{asset('/img/flag/'.$lang_pic)}}" alt="..." class="img-thumbnail icon-small">  
                        <span id="lanNavSel">{{$lang_name}}</span> <span class="caret"></span></a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a id="navIta" href="#" class="dropdown-item language">

                                <img id="imgNavIta" src="{{asset('/img/flag/ita_40.jpg')}}" alt="..." class="img-thumbnail icon-small"> 
                                <span id="lanNavIta">Italiano</span> 
                            </a></li>
                        <li><a id="navDeu" href="#" class="dropdown-item language">

                                <img id="imgNavDeu" src="{{asset('/img/flag/deu_40.jpg')}}" alt="..." class="img-thumbnail icon-small"> 
                                <span id="lanNavDeu">Deutsch</span> 
                            </a></li>
                        <li><a id="navFra" href="#" class="dropdown-item language">

                                <img id="imgNavFra" src="{{asset('/img/flag/fra_40.jpg')}}" alt="..." class="img-thumbnail icon-small"> 
                                <span id="lanNavFra">Francais</span> 
                            </a></li>
                        <li><a id="navEng" href="#" class="dropdown-item language">

                                <img id="imgNavEng" src="{{asset('/img/flag/eng_40.jpg')}}" alt="..." class="img-thumbnail icon-small"> 
                                <span id="lanNavEng">English</span> 
                            </a></li>
                        <li><a id="navRus" href="#" class="dropdown-item language">
                                <img id="imgNavRus" src="{{asset('/img/flag/rus.jpg')}}" alt="..." class="img-thumbnail icon-small"> 
                                <span id="lanNavRus">Russia</span> 
                            </a></li>
                    </ul>
                </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
