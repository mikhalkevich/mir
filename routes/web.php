<?php
use App\Http\Controllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Controllers\BaseController::class, 'getIndex'])->middleware('cookie');

Auth::routes();

Route::prefix('home')->group(function(){
    Route::controller(Controllers\HomeController::class)->group(function () {
        Route::get('/', 'index')->name('home');
    });
    Route::controller(Controllers\HomeEventController::class)->group(function(){
        Route::get('events', 'getIndex');
        Route::post('event', 'postIndex');
    });
});
Route::get('events', [Controllers\EventController::class, 'getIndex']);
Route::get('event/{event}', [Controllers\EventController::class, 'getOne']);
Route::controller(Controllers\CountryController::class)->prefix('country')->group(function(){
    Route::prefix('ajax')->group(function(){
        Route::post('cities', 'postAjaxCities');
        Route::post('news', 'postAjaxNews');
        Route::post('links', 'postAjaxLinks');
    });
    Route::get('{url}', 'getName')->where('url', '[A-Za-z]{3}')->middleware('cookie');
});




